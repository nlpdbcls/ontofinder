#!/usr/bin/env ruby
require 'parseconfig'
require 'optparse'
require 'dbi'


## config file processing
config = ParseConfig.new('ontofinder.cfg')
dbid   = config['DBID'];
dbpw   = config['DBPassword'];
dbname = config['DBName'];


## command line option processing
option = {}

optparse = OptionParser.new do|opts|
  opts.banner = "Usage: dicdb-create.rb [options]"

  option[:recreate] = false
  opts.on('-r', '--recrete', 'tells it to delete current tables and recreate new ones.') do
    option[:recreate] = true
  end

  opts.on('-h', '--help', 'displays this screen') do
    puts opts
    exit
  end
end

optparse.parse!

dbh = DBI.connect("DBI:Pg:#{dbname}", dbid, dbpw)

if option[:recreate] then
  dbh.do('DROP TABLE IF EXISTS ontologies, terms, expressions')
end

dbh.do <<SQL
  CREATE TABLE ontologies (
    vid      TEXT PRIMARY KEY,
    oid      TEXT,
    name     TEXT,
    abbr     TEXT,
    category TEXT 
  );
SQL

dbh.do <<SQL
  CREATE TABLE terms (
    id          SERIAL PRIMARY KEY,
    uri         TEXT,
    tid         TEXT,
    label       TEXT,
    ontology_id TEXT
  );
SQL

dbh.do <<SQL
  CREATE TABLE expressions (
    id          SERIAL PRIMARY KEY,
    expression  TEXT,
    exp_head    TEXT,
    type        TEXT,
    term_id     INTEGER,
    ontology_id TEXT
  );
SQL
