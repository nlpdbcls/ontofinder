#!/usr/bin/env ruby
require 'parseconfig'
require 'optparse'
require 'dbi'
require './normalizer'


def get_entries (c_node, label)
  e_node = c_node.find_first %Q(./relations/entry[string = '#{label}'])
  if e_node.nil?
    []
  else
    s_node = e_node.find('./list/string')
    s_node.collect {|s| s.content}
  end
end


## config file processing
config  = ParseConfig.new('ontofinder.cfg')
dicdir  = config['OntologyDir']
dbid    = config['DBID']
dbpw    = config['DBPassword']
dbname  = config['DBName']


## command line option processing
optparse = OptionParser.new do |opts|
  opts.banner = "Usage: term-populate.rb [options] < vid-containing-file"

  opts.on('-d', '--dicdir Dir', "specifies the directory with dictionaries (default = #{dicdir}).") do |dir|
    dicdir = dir
  end

  opts.on('-h', '--help', 'displays this screen') do
    puts opts
    exit
  end
end

optparse.parse!

puts dicdir

## prepare database access
dbh = DBI.connect("DBI:Pg:#{dbname}", dbid, dbpw)
dbh_insert_term       = dbh.prepare("INSERT INTO terms       (uri, tid, label, ontology_id) VALUES (?, ?, ?, ?)")
dbh_insert_expression = dbh.prepare("INSERT INTO expressions (expression, exp_head, type, term_id, ontology_id) VALUES (?, ?, ?, ?, ?)")
dbh_get_tid           = dbh.prepare("SELECT id FROM terms WHERE uri = ?")

# take ontology acronyms from the argument list
ARGV.each do |o|
  warn "processing #{o}";

  fname = "#{dicdir}/#{o}.dic"
  File.foreach(fname) do |l|
    exp, uri, label = l.split(/\t/)

    exp.strip!
    uri.strip!
    label.strip!
    type = (exp == label)? 'EXACT' : 'SYNONYM'

    dbh_get_tid.execute(uri)
    r = dbh_get_tid.fetch

    if r.nil?
      dbh_insert_term.execute(uri, uri, label, o);

      dbh_get_tid.execute(uri)
      r = dbh_get_tid.fetch
    end

    abort "strange" if r.nil?
    tid = r[0]

    dbh_insert_expression.execute(exp, Normalizer.get_head(exp), type, tid, o)
  end
  warn "done";
end
