#!/usr/bin/env ruby
require 'lingua/stemmer'

PREPS = ['of', 'in', 'by', 'with', 'for', 'from', 'to', 'before', 'after', 'above', 'beneath', 'into']
PATTERN = ', | ' + PREPS.join(' | ') + ' '
PATTERN_ENUM = 'alpha|beta|gamma|delta|epsilon|zeta|eta|theta|iota|kappa|lambda|mu|nu|xi|omicron|pi|row|sigma|tau|upsilon|phi|chi|psi|omega|i|ii|iii|iv|v|vi|vii|viii|ix|x|[0-9]+'
PATTERN_SORT = 'type|class'

module Normalizer
  def Normalizer.get_head(str)
    return '' if str.nil? || str.empty?

    str.downcase!
    
    ## get the head token

    # get the first clause except ...
    clauses = str.split(/, /)
    hclause = ((clauses.first =~ /ed$/) && (clauses.last !~ /ed$/))? clauses.last : clauses.first

    # get the first phrase except ...
    phrases = hclause.split(/(#{PATTERN})/)
    hphrase = phrases[0]
    hphrase = phrases[2] if ((phrases.length > 2) and (phrases[0] == 'type') and (phrases[1] == ' of '))
    hphrase = phrases[2] if ((phrases.length > 2) and (phrases[0] =~ /^type (#{PATTERN_ENUM})$/) and (phrases[1] == ' of '))

    # get the last token except ...
    tokens = hphrase.gsub(/[-_.,:;\/&|(){}<>"' ]+/, ' ').split
    htoken = tokens[-1]
    htoken = tokens[-2] if ((tokens.length > 1) and (tokens[-1] =~ /^(#{PATTERN_ENUM})$/))
    htoken = tokens[-3] if ((tokens.length > 2) and (tokens[-1] =~ /^(#{PATTERN_ENUM})$/) and (tokens[-2] =~ /^(#{PATTERN_SORT})$/))

    htoken
  end

  def Normalizer.norm(str)
    str.downcase!
    
    ## get the head token
    clauses = str.split(/, /)
    hclause = ((clauses.first =~ /ed$/) && (clauses.last !~ /ed$/))? clauses.last : clauses.first
    phrases = hclause.split(/(#{PATTERN})/)
    tokens = phrases.first.gsub(/[-_.,:;\/&|(){}<>"' ]+/, ' ').split
    htoken = tokens[-1]
    if htoken =~ /^(#{PATTERN_ENUM})$/ then htoken = tokens[-2] end

    parts = str.split(/#{PATTERN}/)
    toks = []
    parts.each do |p|
      unless (p =~ /#{PATTERN}/)
        toks.concat(p.gsub(/[-_.,:;\/&|(){}<>"' ]+/, ' ').split)
      end
    end

    ntoks = toks.collect{|t| Lingua.stemmer(t, :language => 'en')}
#    "#{htoken}^#{Lingua.stemmer(htoken, :language => 'en')}^#{ntoks.join}"
    ntoks.join
  end
end

if __FILE__ == $0
  ARGF.each do |s|
    s.chomp!
#    puts Normalizer.norm(s)
    puts Normalizer.get_head(s)
  end
end
