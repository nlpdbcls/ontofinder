#!/usr/bin/env ruby
require 'parseconfig'
require 'dbi'


## config file processing
config = ParseConfig.new('ontofinder.cfg')
dbid   = config['DBID']
dbpw   = config['DBPassword']
dbname = config['DBName']

dbh = DBI.connect("DBI:Pg:#{dbname}", dbid, dbpw)
dbh_insert_ontology = dbh.prepare("INSERT INTO ontologies VALUES (?, ?, ?, ?, ?)")

while (inline = gets)
  inline.chomp!
  abbr, name = inline.split(/\t/)
  dbh_insert_ontology.execute(abbr, abbr, name, abbr, '-')
end
