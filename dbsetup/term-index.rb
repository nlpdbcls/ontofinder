#!/usr/bin/env ruby
require 'parseconfig'
require 'optparse'
require 'dbi'
require './normalizer'


## config file processing
config  = ParseConfig.new('ontofinder.cfg')
dbid    = config['DBID']
dbpw    = config['DBPassword']
dbname  = config['DBName']


## command line option processing
recreate = false
optparse = OptionParser.new do |opts|
  opts.banner = "Usage: term-index.rb [options]"

  opts.on('-r', '--recrete', 'tells it to refresh the whole index.') do
    recreate = true
  end

  opts.on('-h', '--help', 'displays this screen') do
    puts opts
    exit
  end
end

optparse.parse!

## prepare database access
dbh = DBI.connect("DBI:Pg:#{dbname}", dbid, dbpw)

## set tsvectors
warn "generating tsvectors"
dbh.do("ALTER TABLE expressions DROP COLUMN IF EXISTS exp_search")
dbh.do("ALTER TABLE expressions ADD COLUMN exp_search TSVECTOR")
dbh.do("UPDATE expressions SET exp_search = to_tsvector('english', coalesce(expression, ''))")

#dbh.do(<<SQL
#UPDATE expressions SET exp_search = 
#  setweight(to_tsvector('english', coalesce(expression, '')), 'D') ||
#  setweight(to_tsvector('english', coalesce(head, '')), 'A')
#SQL
#)

## index
warn "generating index"
dbh.do("CREATE INDEX exp_search_idx ON expressions USING GIN(exp_search)")
