$(function() {

	var dragNumber = 0;// dragしたdatumの数
	//var gridX; // snapする座標
	var h = $('html, body'); // body要素
	
	///////////////// boot strap start ///////////
	
	// show hide columnsの中身をつくる
	function makeShowHideColumns() {
		var htmlStr = '<table><tr><th>Show/Hide Ontology</th></tr><tr><td><label><input type="checkbox" checked id="allOntoCheckbox">Check/Uncheck All</lable></td></tr><tr><td><hr/></td></tr>';
	
		var ontos = $('#row0 > th').map(function() {
      		return $.trim($(this).text()) + "|" + $(this).css('display'); 
    	}).get();
		
		$.each(ontos, function(index, obj) {
			var title = obj.split('|')[0];
			var display = obj.split('|')[1];

			if(index > 1) {

				if (display == "none") {
					htmlStr = htmlStr + '<tr><td><label><input type="checkbox" class="checkOnto">' + title + '</label></td></tr>';			
				} else {
					htmlStr = htmlStr + '<tr><td><label><input type="checkbox" class="checkOnto">' + title + '</label></td></tr>';			
				}
			}	
		});

		htmlStr = htmlStr + '</table>';
		$('#showHideColumnsWindow').empty().append(htmlStr);
	}

	makeShowHideColumns();

	// show hide rowsの中身を作る
	function makeShowHideRows() {
		var htmlStr = '<table><tr><th>Show/Hide Term</th></tr><tr><td><label><input type="checkbox" checked id="allTermCheckbox">Check/Uncheck All</lable></td></tr><tr><td><hr/></td></tr>';
		var terms = $('table.data tr').map(function() {
			var obj = $('td:first', $(this));
			return $.trim(obj.text()) + "|" + obj.css('display') + "|" + $(this).attr('id');		
		}).get();

		$.each(terms, function(index, obj) {
			var title = obj.split('|')[0];
			var display = obj.split('|')[1];
			var id = obj.split('|')[2];

			if(index > 0) {
				if (display == "none") {
					htmlStr = htmlStr + '<tr><td><label><input type="checkbox" class="checkTerm" name="' + id + '">' + title +'</label></td></tr>';		
				} else {
					htmlStr = htmlStr + '<tr><td><label><input type="checkbox" class="checkTerm" checked name="' + id + '">' + title +'</label></td></tr>';		
				}
			}	
		});
		htmlStr = htmlStr + '</table>';
	
		$('#showHideRowsWindow').empty().append(htmlStr);
	
	}

	makeShowHideRows();

	// table.data thにidを追加
    (function(i){
		 $('.data th').map(function(i) { // すべての<th>要素を順に処理
	   		return $(this).attr('id', 'col' + i); 
    	});	
	})();

	// showHideColumsWindow に属性をつける	
	(function(i) {
		$('.showHideColumnsWindow input.checkOnto').map(function(i) {
				return $(this).attr('checked', 'checked').attr('name', 'col' + (i+2));
		});
	})();
	
	(function() {
		var trs = $('tr', '.data');
		for(var i = 0; i < trs.length; i++) {
			$('td:nth-child(1)', trs[i]).addClass('ontoTerm');		
		}
	})();

	
	
	// ハサミベースの位置と幅設定
	(function() {
		var xpos = $('table.data tr th').eq(0).width() + $('table.data tr th').eq(1).width();
		//console.log("table width:" + $('table.data').width() );
		//console.log("width:" + xpos + ":" + $('#contents').css('margin-left'));
		$('#scissorsBase').width($('table.data').width());
		gridX = $('table.data tr th').eq(2).width() + 1; // tdの幅と枠線幅
	})();

	// フローティングカラムheaderの構築
    (function(){
		var htmlStr = '<table class="data" id="floatingColumnHeader"><tr>';
		
		var ontos = $('#row0 > th').map(function() {
      		return $.trim($(this).text()) + "|" + $('a', this).attr('href') + "|" + $('a', this).attr('title') + "|" + $(this).css('display'); 
    	}).get();
		
		$.each(ontos, function(index, obj) {
			var ontoName = obj.split('|')[0];
			var link = obj.split('|')[1];
			var title = obj.split('|')[2];
			var display = obj.split('|')[3];
			if(index == 0) {
					htmlStr += '<th>' + ontoName + '</th>';	
			} else if(index == 1) {
					htmlStr += '<th class="dropArea">' + ontoName.replace("All clear", "") + '<div class="allClearBtn">All clear</div></th>';		
			} else {
					htmlStr += '<th><div class="f_allDropBtn"><img src="images/drop_btn.jpg"></div><div class="f_hideBtn"><img src="images/close_btn.gif"/></div><a href="' + link + '" title="' + title + '">' + ontoName + '</a></th>';		
			}
		});
		htmlStr = htmlStr + '</tr></table>';
	
		$('#floatingColumn').empty().append(htmlStr);
	
	})();

	// フローティングカラムheaderのthにidを追加
    (function(i){
		 $('#floatingColumnHeader th').map(function(i) { // すべての<th>要素を順に処理
	   		return $(this).attr('id', 'f_col' + i); 
    	});	
	})();	

	var menuYloc = parseInt($("#floatingColumn").css("top").substring(0,$("#floatingColumn").css("top").indexOf("px")));
	$('#floatingColumn').hide();

	$(".data th a").tooltip({position: 'bottom left'});
	$("#scissors").tooltip({position: 'bottom left'});

	// フローティングローヘッダーの作成
	function makeFloatingRowHeader() {
		var htmlStr = '<table class="data" id="floatingRowHeader">';
		
		var table = $('table.draggable');
		var rowNum = $('tr', table).length;
		
		var colhtml0 = new Array();
		var colhtml1 = new Array();

		// 最初はColumnヘッダーの文字
		colhtml0.push($('#row0 th').eq(0).html());
		colhtml1.push($('#row0 th').eq(1).html());

		for(var i = 1; i < rowNum; i++) {
			var row = $('#row' + i);
			// dropされたdatumがある場合、id属性を変更	
			var dropArea = $('td', row).eq(1);
			var dropDatums = $('.datum', dropArea);
	
			// id属性に一時的にfをつけます
			dropDatums.map(function() { 
				var droppedId = $(this).attr('id');
				$(this).attr('id', 'f_' + droppedId);			
			});
           
			// htmlをコピーします
			colhtml0.push($('td', row).eq(0).html());
			colhtml1.push($('td', row).eq(1).html());		

			// id属性を元にもどします
			dropDatums.map(function() { 
				var droppedId = $(this).attr('id');
				$(this).attr('id', droppedId.substr(2));			
			});	
		}
		//var terms = $('table.data tr th').eq(0);
		
		for(var i = 0; i < rowNum; i++) {
			if(i == 0) {
				htmlStr =	htmlStr + '<tr id="f_row' + i + '"><th class="dummyCell"><div class="dummyDiv" id="dummyDiv' + i + '"></div></th><th>' + colhtml0[i] + '</th><th class="dropArea">' + colhtml1[i] + '</th></tr>';
			} else {
				htmlStr =	htmlStr + '<tr id="f_row' + i + '"><td class="dummyCell"><div class="dummyDiv" id="dummyDiv' + i + '"></div></td><td class="termColumn ontoTerm">' + colhtml0[i] + '<br/></td><td class="dropArea">' + colhtml1[i] + '</td></tr>';
			}		
		}
		htmlStr += '</table>';
		$('#floatingRow').empty().append(htmlStr);


		for(var i = 0; i < rowNum; i++) {
			var origRow = $('#row' + i);	

			var thHeight = parseInt($('th', origRow).height());
			var tdHeight = parseInt($('td', origRow).height());

			// firefox と operaなら
			if(jQuery.support.checkOn && jQuery.support.noCloneEvent) {
				thHeight = thHeight - 1;
				tdHeight = tdHeight - 1;
				//console.log("chrome");
			}
				
			if(i == 0) {
				$('#dummyDiv' + i).height(thHeight);
			} else {
				//console.log("row height:" + parseInt($('td', origRow).height()));
				$('#dummyDiv' + i).height(tdHeight);
			}
		}
		$('#floatingRow').height(parseInt($('table.draggable').height()));		

	}
	makeFloatingRowHeader();
	
	var menuXloc = parseInt($("#floatingRow").css("left").substring(0,$("#floatingRow").css("left").indexOf("px")));
	$('#floatingRow').hide();


	// 使ってない
	function trim(str) {
		return str.replace(/^\s+|\s+$/g, "");
	}

	////////////////// boot strap end ////////////////
	////////////////////////////////////////////////////////


	// show/hide windowが表示されていたら隠す
	function hidePanel() {
		$("#showHideColumnsWindow").fadeOut("slow");
		$("#showHideRowsWindow").fadeOut("slow");
	}


   	$('#contents *').click(function(e) {
		hidePanel();
	});


   	$('.title').click(function(e) {
		hidePanel();
	});


	
	// windowの縦スクロールに従って、フローティングカラムヘッダーを移動
    $(window).scroll(function( ) {
     	
       var offset = menuYloc+$(document).scrollTop();  
       $("#floatingColumn").animate({top:offset + 'px'},{duration:100,queue:false});
		
		if( offset > 80) {
			$("#floatingColumn").show();		
			$('#scissors').css('opacity', 0.3);
			$('#scissors').draggable('disable');
		} else {
			$("#floatingColumn").hide();
			$('#scissors').css('opacity', 1);
			$('#scissors').draggable('enable');
		}

    });

	// windowの横スクロールに従って、フローティングローヘッダーを移動
    $(window).scroll(function( ) {
 
		var offset = $(document).scrollLeft();
	
		//console.log("offset:" + offset);
       $("#floatingRow").animate({left:offset + 'px'},{duration:100,queue:false});
		
		if( offset > 70) {
			$("#floatingRow").show();		
		} else {
			$("#floatingRow").hide();
		}

    });


	// ハサミ
	$('#scissors').draggable({
		cursor: 'move',

		//grid: [100, 0]	
       helper: "clone", 
        //opacity: 0.7,
       revert: 'invalid',
		start: function(e, ui) {
            ui.helper.css('opacity', 1);
			ui.helper.addClass('scissors');
			ui.helper.prevObject.addClass('moving_origin');
			hidePanel();
		},
		stop: function(e, ui) {
			ui.helper.prevObject.removeClass('moving_origin');
		},
		
	});


	// 隠すthの配列
	var ths;
	var floatThs;
	// ハサミをドロップ
    $('table.data tr th').droppable({
        // ontologyのカラムのみドロップ可能
        accept: function(draggable) {
	          if ($('div',this).hasClass('allDropBtn'))  {
                return true;
            }
        },
       tolerance: "fit",              // Draggable要素が完全に入った場合にDrop可能にする
        //activeClass: "orange-active",  // Draggable要素がドラッグしているときに適用するクラス
       //hoverClass: "orange-hover",    // Draggable要素が上に乗ったときに適用するクラス
		over: function(event, ui) {
			var left = ui.position.left;
			//$('table.data tr th').css('background-color', '#E2EBF0');
			ths = $('table.data tr th').filter(function(index) {
				//console.log(index);
      			// 条件によるフィルタ
      			return $(this).offset().left + $(this).width() >= left;
	    	}).css('background-color', '#ccc'); // 背景を設定
		},
       drop: function(event, ui) {

			var left = ui.position.left;
			

			ths = $('table.data tr th').filter(function(index) {
					//console.log(index);
      				// 条件によるフィルタ
      				return $(this).offset().left + $(this).width() >= left;
	    		}).css('background-color', '#ccc'); // 背景を設定
		
			$('#confirmPanel').css('left', ui.position.left + 'px');
			$('#confirmPanel').css('top', parseInt(ui.position.top + 50) + 'px');

			$('#confirmPanel').fadeIn("fast");
        },
		out: function(event, ui) {
			$('table.data tr th').css('background-color', '#E2EBF0');	
		}
    });

 	$('#confirmPanel input').click(function() {
		//console.log($(this).val());
		if($(this).val() == "OK") {
			for(var i = 0; i < ths.length; i++) {
				
				var th = ths[i];
				var cols = getCol(th, $('.data'));		
				
				for(var j = 0; j < cols.length; j++) {
					//console.log(cols[j]);
					cols[j].hide('fast');		
				}
				// checkboxのチェックを外す
				//console.log($('input[name="' + $(this).parent().attr('id') + '"]'));
				$('input[name="' + $(th).attr('id') + '"]').removeAttr('checked');
				//return true;

				// フローティングカラムが見えているか
				if($('table th#f_' + $(th).attr('id')).is(':visible')) {
					$('table th#f_' + $(th).attr('id')).hide('slow');
				} else {
					$('table th#f_' + $(th).attr('id')).css('display', 'none');
				}
			
				// chrome bug対策
				$('table.data').width(100);		
				
				if(!IsAllOntoChecked() && !IsAllOntoUnchecked()) {
					$('#allOntoCheckbox').css('opacity', '0.5');
					$('#allOntoCheckbox').attr('checked', 'checked');	
				} else {
					$('#allOntoCheckbox').css('opacity', '1');
					if(IsAllOntoUnchecked()) {
						$('#allOntoCheckbox').removeAttr('checked');		
					} else {
						$('#allOntoCheckbox').attr('checked', 'checked');
					}
				}

			}
			
		} else {
			$('table.data tr th').css('background-color', '#E2EBF0');
		}

		$(this).parent().parent().hide();
	});
	

	
	// datum drag
    $('.datum').draggable({
		 cursor: 'move',
        helper: 'clone', 
        //opacity: 0.7,
        revert: 'invalid',
         start: function(e, ui) {
              ui.helper.addClass('moving');	
				ui.helper.prevObject.addClass('moving_origin');	
				hidePanel();

		},
         stop: function(e, ui) {
				//ui.helper.addClass('moving');	
				ui.helper.prevObject.removeClass('moving_origin');		
		}
    });
    $('.dropArea').droppable({
        // 同じ行のみDrop可能にする
        accept: function(draggable) {

            // 同じ行からのみDrop可能
            if ($(this).parent().attr('id') == draggable.parent().parent().attr('id'))  {
                return true;
            }
        },
        //tolerance: "fit",              // Draggable要素が完全に入った場合にDrop可能にする
        //activeClass: "orange-active",  // Draggable要素がドラッグしているときに適用するクラス
        hoverClass: "orange-hover",    // Draggable要素が上に乗ったときに適用するクラス
        drop: function(event, ui) {
          //  console.log('drop!');
			var clone = ui.draggable.clone();
			
           // get column number of the drag element belongs to
			var columnNum = $('.data td').index(ui.draggable.parent());

			var rowNum = $('.data tr').index(ui.draggable.parent().parent());
            //console.log('columnNum: ' + columnNum);
            //console.log('rowNum: ' + rowNum);


              // onto name 取得
			var ontoName = getOntoName(columnNum, rowNum); 
            // ontology nameとclose btnを追加
			clone.prepend(ontoName + '<div class="removeBtn"><img src="images/close_btn.gif" class=""></div><br/>');

			var child = $(".ontoUri", clone);
			clone.append('<input type="hidden" name="import[]" value="' + child.text() + '"/>');

			// dropBtnを削除
			$('.dropBtn', clone).remove();

			clone.addClass('moving');
			clone.removeClass('moving_origin');	
			clone.appendTo(this);
            // dropした順番をつける
			clone.attr('id', 'dropped' + dragNumber);
            // どのontologyからドラッグしたか、ontologyの左からの番号をつける
			clone.append('<span class="order">' + columnNum + '</span>');
			ui.draggable.attr('id', 'dragged' + dragNumber);

			dragNumber++;

	       ui.draggable.addClass('dragged');
			ui.draggable.draggable("disable");
          //  console.log('ui.draggable.children():' + ui.draggable.children());
 	    	
			// floating rowの再構築
			makeFloatingRowHeader();

			var droppedList = sortByOrder(this);

	    	$(this).empty().append(droppedList);

        },

        over: function(event, ui) {
            // console.log('over');
        }
    });

    // onto nameの取得
     // 行番号と列番号より
    function getOntoName(columnNum, rowNum) {

       if(columnNum > $('.data tr#row0 th').size()) {
   		    columnNum = columnNum - ((rowNum - 1) * $('.data tr#row0 th').size());
        }
       var ontoName = $(".data tr#row0 th:nth-child(" + (columnNum+1) + ")").text();
		//console.log('ontoName:' + ontoName);
　　　　return ontoName;　　　
	}


　　// ドロップしたオブジェクトを<span class="order"></span>の順番でソート
    function sortByOrder(obj) {
    	var droppedList = $('.order', obj).parent().sort(function(a, b){
       	//console.log($('.order', a).text()+ "," + $('.order', b).text());        
          	return $('.order', a).text() - $('.order', b).text();
	    });
		/*        
		for( obj in droppedList) {
			//console.log("droppedList:" + obj);
		}
		*/
		return droppedList;
     }

	// dragされて、元の場所に薄くの残っている要素は
    // a タグを不活性にする
	$("a").click(function(event){
  		if( $(this).parent().hasClass('dragged') ) {
			event.preventDefault();
		} else {
			return;		
		}
	});

	// dragされて、元の場所に薄くの残っている要素は
    // dropBtn タグを不活性にする

	// drop btn click
	$('.dropBtn').click(function(event) {

		if( $(this).parent().hasClass('dragged') ) {
			//event.preventDefault();
		} else {

			//console.log('click!');
          //console.log($(this).parent());
			
			copyDatum($(this).parent());
			// floating rowの再構築
			makeFloatingRowHeader();
			
			return;		
		}
			
	});

	// 	datumをコピーする
	function copyDatum(origObj) {
		// $(this).parent() == origObj

		var clone = origObj.clone();
       //console.log(clone);

       var columnNum = $('.data td').index(origObj.parent());
		//console.log("columnNum" + columnNum);
		var rowNum = $('.data tr').index(origObj.parent().parent());
		//console.log("rowNum:" + rowNum);
			
		var ontoName = getOntoName(columnNum, rowNum); 
       // ontology nameとclose btnを追加
       clone.prepend(ontoName + '<div class="removeBtn"><img src="images/close_btn.gif" class=""></div><br/>');

       var child = $(".ontoUri", clone);

       clone.append('<input type="hidden" name="import[]" value="' + child.text() + '"/>');

		// dropBtnを削除
		$('.dropBtn', clone).remove();
		clone.addClass('moving');	

		// dropAreaに追加
       clone.appendTo($('#row' + rowNum + ' > td.dropArea'));
			
		// dropした順番をつける
       clone.attr('id', 'dropped' + dragNumber);
     	// どのontologyからドラッグしたか、ontologyの左からの番号をつける
       clone.append('<span class="order">' + columnNum + '</span>');
		origObj.attr('id', 'dragged' + dragNumber);

       dragNumber++;

       origObj.addClass('dragged');
       origObj.draggable("disable");
       //console.log('ui.draggable.children():' + origObj.children());
       var droppedList = sortByOrder($('#row' + rowNum + ' > td.dropArea'));
                   
      	// console.log($('#row' + rowNum + ' > td.dropArea'));
             
		$('#row' + rowNum + ' > td.dropArea').empty().append(droppedList);			
	}


	// remove clone object when removeBtn clicked
    $(".removeBtn").live('click', function(){
		//console.log($(this).parent().attr('id').replace('dropped', 'dragged'));
			
		// 本物dropAreaのdatumaの削除ボタンか？
		if(	$(this).parent().attr('id').substr(0,1) == "f") {
			// from floating
			$('#' + $(this).parent().attr('id').substr(2).replace('dropped', 'dragged')).removeClass('dragged').draggable("enable");
			$('#' + $(this).parent().attr('id').substr(2)).remove();
		} else {
			// form real
			$('#' + $(this).parent().attr('id').replace('dropped', 'dragged')).removeClass('dragged').draggable("enable");
			$('#f_' + $(this).parent().attr('id')).remove();
		} 	
 
       //console.log('remove datum:' + $('#' + $(this).parent().attr('id')));

       $(this).parent().remove();

       return false;
    });

	$('.allClearBtn').live("click", function() {
			var columnNum = $('.data th').index($(this).parent());
			//console.log("all clear:" + columnNum);
						
			$('td.dropArea').empty();
			$('.datum').removeClass('dragged').draggable("enable").removeAttr('id');
	});

	// showHideColumnsWindowの表示
	$('.showHideColumnsWindowBtn').click(function() {
		
		if ($('#showHideColumnsWindow').css('display') == "none") {
			
			$('.title').unbind('click');

			if($('#showHideRowsWindow').css('display') == "block") {
				$("#showHideRowsWindow").fadeOut("slow");	
			}

       	$("#showHideColumnsWindow").fadeIn("slow", function() {
				// div.titleにクリックイベントをバインド	
				$('.title').bind('click', function(e) {
						$("#showHideColumnsWindow").fadeOut("slow");
						$("#showHideRowsWindow").fadeOut("slow");		
				} );
			});
			$('#showHideColumnsWindow').css('top', 40);
 			//$('#showHideColumnsWindow').css('left', ($('html').width() - $('#showHideColumnsWindow').width() - $('#showHideRowsWindow').width()  - 100));
			$('#showHideColumnsWindow').css('left', ($('html').width() - $('#showHideColumnsWindow').width() - 60));
		} else {
				

			$("#showHideColumnsWindow").fadeOut("slow");
		}	
    });

	// showHideRowsWindowの表示
	$('.showHideRowsWindowBtn').click(function() {
 		//console.log("showHideRowsWindowBtn :" + $('.showHideRowsWindowBtn').position().left + "," + $('.showHideRowsWindowBtn').position().top);
    	//console.log("window width :" + $('html').width());
		if ($('#showHideRowsWindow').css('display') == "none") {
		
			$('.title').unbind('click');

			if($('#showHideColumnsWindow').css('display') == "block") {
				$("#showHideColumnsWindow").fadeOut("slow");	
			}

       	$("#showHideRowsWindow").fadeIn("slow", function() {
				// div.titleにクリックイベントをバインド	
				$('.title').bind('click', function(e) {
					$("#showHideColumnsWindow").fadeOut("slow");
					$("#showHideRowsWindow").fadeOut("slow");	
				});		
			});
			$('#showHideRowsWindow').css('top', 40);
 			$('#showHideRowsWindow').css('left', ($('html').width() - $('#showHideRowsWindow').width() - 60));
		} else {
			$("#showHideRowsWindow").fadeOut("slow");
		}	
    });

	// 列を隠すボタンクリック
	$('.hideBtn').click(function(e, param) {
		
		var thObj;

		if (param != undefined) {
			thObj = $($('#' + param), $('.data'));
		} else {
			thObj = $(this).parent();
		}
		
		var cols = getCol(thObj, $('.data'));

		
		
		//console.log("col length: " + cols.length);
		for(var i = 0; i < cols.length; i++) {
 			//console.log(cols[i]);
			cols[i].hide('slow', function() {
				//console.log("hide col:" + j);
				makeFloatingRowHeader();
			});	
		}
		// checkboxのチェックを外す
		//console.log($('input[name="' + thObj.attr('id') + '"]'));
       $('input[name="' + thObj.attr('id') + '"]').removeAttr('checked');
		
		// chrome bug対策
		$('table.data').width(100);

		if(!IsAllOntoChecked() && !IsAllOntoUnchecked()) {
			$('#allOntoCheckbox').css('opacity', '0.5');
			$('#allOntoCheckbox').attr('checked', 'checked');	
		} else {
			$('#allOntoCheckbox').css('opacity', '1');
			if(IsAllOntoUnchecked()) {
				$('#allOntoCheckbox').removeAttr('checked');		
			} 
		}

	});

	// すべてドロップするボタンクリック
	$('.allDropBtn').click(function(e, param) {
		//console.log("all drop clicked!");

		var thObj;

		if (param != undefined) {
			thObj = $($('#' + param), $('.data'));
		} else {
			thObj = $(this).parent();
		}
		
		var cols = getCol(thObj, $('.data'));


		//var cols = getCol($(this).parent(), $('.data'));
		//console.log(cols);

		// 0番目はthなので、1から始まる
		for (var i = 1; i < cols.length; i++) {
			// 2重コピーを防ぐため、class = 'dragged'以外を選択
			//console.log($('div.datum', cols[i]).not('.dragged'));	
			var datumArray = $('div.datum', cols[i]).not('.dragged');						
			for(var j = 0; j < datumArray.length; j++) {
				copyDatum($(datumArray[j]));			
			}
		}

		// floating rowの再構築
		makeFloatingRowHeader();
	});

	$('.f_hideBtn').live('click', function() {
       var id = $(this).parent().attr('id').substr(2);
		$(this).parent().hide();
		//var targetObj = $('.data th#' + id);
		$('.hideBtn').trigger('click', [id]);
	});

	$('.f_allDropBtn').live('click', function() {
       var id = $(this).parent().attr('id').substr(2);
		$('.allDropBtn').trigger('click', [id]);
	})

	/* 列を取得する */
	function getCol(cell,table){
		var tds = [];
		var index = $("th",$(cell).parent()).index(cell);
		//console.log(index);

		var tr = $("tr",table);

		tds.push($("th:nth-child("+(index+1)+")",tr[0]));
		for(var i = 1; i < tr.length; i++){
			tds.push($("td:nth-child("+(index+1)+")",tr[i]));
		}
		return tds;
	}


	//////////////////////////
	// input.checkOntoの状態
	var isOntoMouseDown = false;
	// mouse downした最初のobject
   	var startOntoObj = null;
	// すべてチェックされているか、またはすべてチェックされていないか
 	//var isAllOntoCheck = ture;

	
	$('label:has(input.checkOnto)').live('mousedown', function(e) {
		isOntoMouseDown = true;		
		startOntoObj = $('input.checkOnto', this);
		//console.log("start:" + startOntoObj.attr('name'));
	
	}).live('mouseover', function(e) {
		if(isOntoMouseDown) {

			if(startOntoObj != null) {
				if(startOntoObj.attr('checked') == 'checked') {
					startOntoObj.removeAttr('checked');
				} else {
					startOntoObj.attr('checked', 'checked');
				}
				hideColumns(startOntoObj);
				startOntoObj = null;
			}

			if($('input.checkOnto', this).attr('checked') == 'checked') {
				$('input.checkOnto', this).removeAttr('checked');
			} else {
				$('input.checkOnto', this).attr('checked', 'checked');
			}
			hideColumns($('input.checkOnto', this));
		}
	}).live('mouseup', function(e) {
		isOntoMouseDown = false;
		
		if(!IsAllOntoChecked() && !IsAllOntoUnchecked()) {
			$('#allOntoCheckbox').css('opacity', '0.5');
			$('#allOntoCheckbox').attr('checked', 'checked');	
		} else {
			$('#allOntoCheckbox').css('opacity', '1');
			if(IsAllOntoUnchecked()) {
				$('#allOntoCheckbox').removeAttr('checked');		
			} 
		}
	});

    $('body').mouseup(function(e) {
		isOntoMouseDown = false;	
		isTermMouseDown = false;	
	});

	$('input.checkOnto').live('click', function() {
		hideColumns($(this));

		if(!IsAllOntoChecked() && !IsAllOntoUnchecked()) {
			$('#allOntoCheckbox').css('opacity', '0.5');
			$('#allOntoCheckbox').attr('checked', 'checked');	
		} else {
			$('#allOntoCheckbox').css('opacity', '1');
			if(IsAllOntoUnchecked()) {
				$('#allOntoCheckbox').removeAttr('checked');		
			} else {
				$('#allOntoCheckbox').attr('checked', 'checked');
			}
		}
	});

	$('#allOntoCheckbox').live('click', function(e) {
		if($(this).css('opacity') != "1") {
			e.preventDefault();
			$(this).css('opacity', '1');
			$('.showHideColumnsWindow input.checkOnto').map(function(i) {
				$(this).attr('checked', 'checked');
				hideColumns($(this));
			});	
			return false;	
		}
			

		if($(this).attr('checked') == 'checked') {
			$('.showHideColumnsWindow input.checkOnto').map(function(i) {
				$(this).attr('checked', 'checked');
				hideColumns($(this));
			});
		} else {
			$('.showHideColumnsWindow input.checkOnto').map(function(i) {
				$(this).removeAttr('checked');
				hideColumns($(this));
			});
		}
		$(this).css('opacity', '1');
	});
	
	// 	すべてのOntoCheckの状態	
	function IsAllOntoUnchecked() {
		if($('.showHideColumnsWindow input.checkOnto:checked').attr('name') == undefined) {
			return true;	
		} else {
			//console.log('checked:' + $('.showHideColumnsWindow input.checkOnto:checked').attr('name'));
			return false;
		}
	}

	// 	すべてのOntoCheckの状態	
	function IsAllOntoChecked() {
		if($('.showHideColumnsWindow input.checkOnto:not(:checked)').attr('name') == undefined) {
			return true;	
		} else {
			//console.log("unchecked:" + $('.showHideColumnsWindow input.checkOnto:not(:checked)').attr('name'));
			return false;
		}
	}


	function hideColumns(obj) {
		if(obj.attr('checked') == undefined) {
			//console.log("uncheck:" + obj.attr('name'));

			var cols = getCol($('th#' + obj.attr('name')), $('.data'));
			//console.log(cols);
			for(var i = 0; i < cols.length; i++) {
	 			//console.log(cols[i]);
				cols[i].hide('slow', function() {
					// floating rowの再構築
					makeFloatingRowHeader();				
				});		
			}

			// フローティングカラムが見えているか
			if($('table th#f_' + obj.attr('name')).is(':visible')) {
				$('table th#f_' + obj.attr('name')).hide('slow');
			} else {
				$('table th#f_' + obj.attr('name')).css('display', 'none');
			}

		} else {
			
			//console.log("check:" + obj.attr('name'));
			var cols = getCol($('th#' + obj.attr('name')), $('.data'));
			//console.log(cols);
			for(var i = 0; i < cols.length; i++) {
	 			//console.log(cols[i]);
				cols[i].removeAttr('style');
				cols[i].show('slow', function() {
					// floating rowの再構築
					makeFloatingRowHeader();				
				});		
			}
			$('table th#f_' + obj.attr('name')).show('slow');
	
		}	
		// chrome bug対策
		$('table.data').width(100);	
	}


	//////////////////////////
	// input.checkTermの状態
	var isTermMouseDown = false;
	// mouse downした最初のobject
   	var startTermObj = null;

	
	$('label:has(input.checkTerm)').live('mousedown', function(e) {
		isTermMouseDown = true;		
		startTermObj = $('input.checkTerm', this);
	
	}).live('mouseover', function(e) {
		if(isTermMouseDown) {
		
			if(startTermObj != null) {
				if(startTermObj.attr('checked') == 'checked') {
					startTermObj.removeAttr('checked');
				} else {
					startTermObj.attr('checked', 'checked');
				}
				hideRows(startTermObj);
				startTermObj = null;
			}

			if($('input.checkTerm', this).attr('checked') == 'checked') {
				$('input.checkTerm', this).removeAttr('checked');
			} else {
				$('input.checkTerm', this).attr('checked', 'checked');
			}
			hideRows($('input.checkTerm', this));
		}
	}).live('mouseup', function(e) {
		if(!IsAllTermChecked() && !IsAllTermUnchecked()) {
			$('#allTermCheckbox').css('opacity', '0.5');
			$('#allTermCheckbox').attr('checked', 'checked');	
		} else {
			$('#allTermCheckbox').css('opacity', '1')
			if(IsAllTermUnchecked()) {
				$('#allTermCheckbox').removeAttr('checked');		
			} else {
				$('#allTermCheckbox').attr('checked', 'checked');
			}
		}
		isTermMouseDown = false;
		
	});
	

	
	$('input.checkTerm').live('click', function() {
		if(!IsAllTermChecked() && !IsAllTermUnchecked()) {
			$('#allTermCheckbox').css('opacity', '0.5');
			$('#allTermCheckbox').attr('checked', 'checked');	
		} else {
			$('#allTermCheckbox').css('opacity', '1')
			if(IsAllTermUnchecked()) {
				$('#allTermCheckbox').removeAttr('checked');		
			} 
		}
		
		hideRows($(this));
		
	});
	
	$('#allTermCheckbox').live('click', function(e) {
		if($(this).css('opacity') != "1") {
			e.preventDefault();
			$(this).css('opacity', '1');
			$('.showHideRowsWindow input.checkTerm').map(function(i) {
				$(this).attr('checked', 'checked');
				hideRows($(this));
			});	
			return false;	
		}
			

		if($(this).attr('checked') == 'checked') {
			$('.showHideRowsWindow input.checkTerm').map(function(i) {
				$(this).attr('checked', 'checked');
				hideRows($(this));
			});
		} else {
			$('.showHideRowsWindow input.checkTerm').map(function(i) {
				$(this).removeAttr('checked');
				hideRows($(this));
			});
		}
		$(this).css('opacity', '1');
	});

	// 	すべてのTermCheckの状態	
	function IsAllTermUnchecked() {
		if($('.showHideRowsWindow input.checkTerm:checked').attr('name') == undefined) {
			//console.log('checked:' + $('.showHideRowsWindow input.checkTerm:checked').attr('name'));
			return true;	
		} else {
			//console.log('checked:' + $('.showHideRowsWindow input.checkTerm:checked').attr('name'));
			return false;
		}
	}

	// 	すべてのTermCheckの状態	
	function IsAllTermChecked() {
		if($('.showHideRowsWindow input.checkTerm:not(:checked)').attr('name') == undefined) {
			//console.log("unckecked:" + $('.showHideRowsWindow input.checkTerm:not(:checked)').attr('name'));
			return true;	
		} else {
			//console.log("unchecked:" + $('.showHideRowsWindow input.checkTerm:not(:checked)').attr('name'));
			return false;
		}
	}
	


	function hideRows(obj) {
		if(obj.attr('checked') == undefined) {
			//console.log("uncheck:" + obj.attr('name'));
			
			var origRow = $('#' + obj.attr('name'));
			var floatRow = $('#' + 'f_' + obj.attr('name')); 
			origRow.hide('slow');		
			
			// フローティングロー	が見えているか
			if(floatRow.is(':visible')) {
				floatRow.hide('slow');
			} else {
				floatRow.css('display', 'none');
	
			}

		} else {
			//console.log("check:" + obj.attr('name'));
				
			var origRow = $('#' + obj.attr('name'));
			var floatRow = $('#' + 'f_' + obj.attr('name')); 		

			origRow.show('slow');
			floatRow.show('slow', function() {
				makeFloatingRowHeader();			
			});
		
		}
				
	}



	// 行を隠すボタンクリック
	$('.rowHideBtn').click(function(e) {
		//console.log($('.data'));
		var row = $(this).parent().parent();
		var index = $('.draggable tr').index(row)		

		if(index > 0) {
			// 	from real row
			//console.log("from real row" );

			// フローティングロー	が見えているか
			if($('#floatingRowHeader tr').eq(index).is(':visible')) {
				$('#floatingRowHeader tr').eq(index).hide('slow');
			} else {
				$('#floatingRowHeader tr').eq(index).css('display', 'none');
			}

			//$('#floatingRowHeader tr').eq(index).hide('slow');
			// checkboxのチェックを外す
			//console.log($('input[name="' + row.attr('id') + '"]'));
      		$('input[name="' + row.attr('id') + '"]').removeAttr('checked');
		
		} else {
			// from froating row
			index = $('#floatingRowHeader tr').index(row);
			$('.draggable tr').eq(index).hide('slow');
			// checkboxのチェックを外す
			//console.log($('input[name="' + row.attr('id').substr(2) + '"]'));
      		$('input[name="' + row.attr('id').substr(2) + '"]').removeAttr('checked');
	
		}
		row.hide('slow');		

		if(!IsAllTermChecked() && !IsAllTermUnchecked()) {
			$('#allTermCheckbox').css('opacity', '0.5');
			$('#allTermCheckbox').attr('checked', 'checked');	
		} else {
			$('#allTermCheckbox').css('opacity', '1')
			if(IsAllTermUnchecked()) {
				$('#allTermCheckbox').removeAttr('checked');		
			} else {
				$('#allTermCheckbox').attr('checked', 'checked');
			}
		}

	});


});
