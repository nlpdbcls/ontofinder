#!/usr/bin/env ruby
#encoding: UTF-8


## config file processing
require 'parseconfig'
config     = ParseConfig.new('./ontofinder.cfg')
dbid       = config['DBID']
dbpw       = config['DBPassword']
dbname     = config['DBName']
maxnum_rel = config['MaxNumMatchingRelative'].to_i
maxnum_abs = config['MaxNumMatchingAbsolute'].to_i
minscore   = config['MinScore'].to_i
devel      = config['DevelopmentMode'].to_i
vids       = config['Ontologies'].split.collect {|id| id.to_i}
examples   = config['Examples']


## initialize ontofinder
require './ontofinder'
ontofinder = Ontofinder.new(dbid, dbpw, dbname)


## web service
require 'sinatra'
require 'sinatra/cross_origin'
require 'rack/conneg'
require 'json'
require 'erb'

use(Rack::Conneg) { |conneg|
  conneg.set :accept_all_extensions, false
  conneg.set :fallback, :html
  conneg.ignore('/css/')
  conneg.ignore('/js/')
  conneg.ignore('/images/')
  conneg.provide([:json, :html])
}

before do
  if negotiated?
    content_type negotiated_type
  end
end

get '/' do
  @examples = examples
  erb :index
end

post '/mappings' do
  terms = params['data'].split(/[\r\n\t]+/).collect {|t| t.strip.gsub(/ +/, ' ')}
  maxnum_rel = params['maxnum_rel'].to_i if params.has_key?('maxnum_rel')
  maxnum_abs = params['maxnum_abs'].to_i if params.has_key?('maxnum_abs')
  minscore   = params['minscore'].to_i if params.has_key?('minscore')
  devel      = params['devel'].to_i if params.has_key?('devel')
  vids       = params['vids'].split.collect {|id| id.to_i} if params.has_key?('vids')

  respond_to do |wants|
    wants.json  {
      results = ontofinder.get_uris(terms, maxnum_rel, maxnum_abs, minscore, vids)
      results.to_json
    }
    wants.html {
      @results = ontofinder.ontofind(terms, maxnum_rel, maxnum_abs, minscore, vids, devel)
      erb :results
    }
    wants.other { 
        content_type 'text/plain'
        error 406, "Not Acceptable" 
    }
  end
end


post '/keywords' do
  cross_origin

  keywords   = params['keywords'].split(/[\r\n\t]+/).collect {|t| t.strip.gsub(/ +/, ' ')}
  maxnum_rel = params['maxnum_rel'].to_i if params.has_key?('maxnum_rel')
  maxnum_abs = params['maxnum_abs'].to_i if params.has_key?('maxnum_abs')
  minscore   = params['minscore'].to_i if params.has_key?('minscore')
  devel      = params['devel'].to_i if params.has_key?('devel')
  vids       = params['vids'].split.collect {|id| id.to_i} if params.has_key?('vids')

  annotatedKeywords, ontologies = ontofinder.get_mappings(keywords, maxnum_rel, maxnum_abs, minscore, vids)
  response = {:annotatedContents=>annotatedKeywords, :ontologies=>ontologies}.to_json

  content_type :json
  response
end


post '/owl' do
  @baseuri = params['uri']
  @terms   = params['import'].collect {|v| v.split('^')}

  erb :ontology
  #puts "Content-Type:application/x-download"
  #puts "Content-Type: application/rdf+xml"
  #puts "Content-Disposition:attachment;filename=#{filename}"
  #puts
  #Ontofactory.owl(uri, import)
end
