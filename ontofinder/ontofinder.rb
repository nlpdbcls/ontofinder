#!/usr/bin/env ruby
require 'dbi'
require '../dbsetup/normalizer'
require './strsim'


class Ontofinder
  def initialize (dbid, dbpw, dbname)
    ## dictionary DB preparation
    begin
      @ddb = DBI.connect("DBI:Pg:#{dbname}", dbid, dbpw)
    rescue DBI::DatabaseError => e
      puts "An error occurred"
      puts "Error code: #{e.err}"
      puts "Error message: #{e.errstr}"
    end


    @ddb_get_exp      = @ddb.prepare("SELECT expression, type, term_id, ontology_id, ts_rank_cd(exp_search, query, 2|4) AS rank
                                      FROM expressions, plainto_tsquery('english', ?) query
                                      WHERE query @@ exp_search ORDER BY rank DESC")
    @ddb_get_exp_vid  = @ddb.prepare("SELECT expression, type, term_id, ontology_id, ts_rank_cd(exp_search, query, 2|4) AS rank
                                      FROM expressions, plainto_tsquery('english', ?) query
                                      WHERE query @@ exp_search AND ontology_id in ?
                                      ORDER BY rank DESC")
#    @ddb_get_exp      = @ddb.prepare("SELECT expression, type, term_id, ontology_id, ts_rank_cd(exp_search, plainto_tsquery('english', ?), 2|4) AS rank
#                                      FROM expressions, to_tsquery('english', ?) query
#                                      WHERE query @@ exp_search ORDER BY rank DESC")
    @ddb_get_term     = @ddb.prepare("SELECT uri, tid, label, ontology_id FROM terms WHERE id = ?")
    @ddb_get_ontology = @ddb.prepare("SELECT name, abbr FROM ontologies WHERE vid = ?")
  end

  def ontofind (strs, maxnum_rel, maxnum_abs, minscore, vids = [], devel = 0)
    @strs = strs
    ontolookup(strs, maxnum_rel, maxnum_abs, minscore, vids)
    ontosort
    ontohtml(devel)
  end
    

  def get_uris (strs, maxnum_rel, maxnum_abs, minscore, vids = [])
    @strs = strs
    ontolookup(strs, maxnum_rel, maxnum_abs, minscore, vids)
    ontosort

    uri_map = Hash.new
    strs.each {|s| uri_map[s] = []}

    @rterms.each do |str, exp, rel, tid, oid, sco|
      @ddb_get_term.execute(tid)
      r = @ddb_get_term.fetch
      uri, tid, label, oid = r[0], r[1], r[2], r[3]
      uri_map[str] << uri
    end

    uris = Array.new
    @strs.each {|s| uris << uri_map[s]}
    uris
  end


  def get_mappings (strs, maxnum_rel, maxnum_abs, minscore, vids = [])
    @strs = strs
    ontolookup(strs, maxnum_rel, maxnum_abs, minscore, vids)
    ontosort

    uri_map = Hash.new
    strs.each {|s| uri_map[s] = []}

    @rterms.each do |str, exp, rel, tid, oid, sco|
      @ddb_get_term.execute(tid)
      r = @ddb_get_term.fetch
      uri, tid, label, oid = r[0], r[1], r[2], r[3]
      uri_map[str] << {:score=>sco, :concept=>{:ontologyId=>oid, :conceptId=>tid, :uri=>uri, :label=>label}}
    end

    annotatedKeywords = []
    @strs.each {|s| annotatedKeywords << {:content=>s, :annotations=>uri_map[s]}}

    ontologies = ontosort

    [annotatedKeywords, ontologies]
  end



  def ontolookup (strs, maxnum_rel = 1, maxnum_abs = 5, minscore = 0, vids = [])
    @rterms = []

    rcount = Hash.new(0)        # count of the result
    mscore = Hash.new(0)        # the minimal score so far
    exactp = Hash.new(false)    # whether 'EXACT' match has been found or not
    foundp = Hash.new(false)    # whether a term is found for a string

    strs.each_with_index do |s, sidx|
#      squery = s.split.join(' | ')
#      @ddb_get_exp.execute(s, squery)
      @ddb_get_exp.execute(s)

      @ddb_get_exp.each do |r|
        exp, type, tid, oid, rank = r[0], r[1], r[2] ,r[3], r[4]
        ## ontology selection
        next if (!vids.empty?) && (!vids.include?(oid))

        okey = s + '-' + oid.to_s # ontology key
        tkey = s + '-' + tid.to_s # term key

        ## to avoid duplicate terms
        next if foundp[tkey]

        ## for tresholding by the score
        next if (minscore > 0) && (rank < minscore)

        ## for tresholding by the number (relative)
        next if (maxnum_rel > 0) && (rcount[okey] >= maxnum_rel) && (rank < mscore[okey])

        ## for tresholding by the number (absolute)
        next if (maxnum_abs > 0) && (rcount[okey] >= maxnum_abs)

        ## skip non-exact matches if exact one is already found.
        next if (type != 'EXACT') && (exactp[okey])

        rcount[okey] += 1
        mscore[okey]  = rank
        exactp[okey]  = true if type == 'EXACT'
        foundp[tkey]  = true

        @rterms.push([s, exp, type, tid, oid, rank])
      end
    end
    @rterms
#    @rterms.each {|t| p t}
#    puts "=-=-=-=-=-=-=-=-=-="
  end


  def ontosort
    omatch = Hash.new {|h,k| h[k]=[]}       # matched strings per ontology
    @rterms.each {|str, exp, rel, tid, oid, sco| omatch[oid] << str}

    ocount   = Hash.new(0) # count the number of strings matched per ontology
    ocount_u = Hash.new(0) # count the number of uniq strings matched per ontology
    omatch.each do |o, sv|
      ocount[o]   = sv.size
      ocount_u[o] = sv.uniq.size
    end

    osseen = Hash.new(FALSE)
    oscore = Hash.new(0)
    @rterms.each do |str, exp, rel, tid, oid, sco|
      okey = str + '-' + oid.to_s # ontology-string key
      unless osseen[okey]
        oscore[oid] += sco
        osseen[okey] = TRUE
      end
    end

    @oids = ocount.keys.sort{|a,b| oscore[b] <=> oscore[a]}
#    @oids = ocount.keys.sort{|a,b| (ocount_u[b] <=> ocount_u[a]).nonzero? || (ocount[b] <=> ocount[a])}

    ontologies = []
    @oids.each do |oid|
      @ddb_get_ontology.execute(oid)
      r = @ddb_get_ontology.fetch
      name, abbr = r[0], r[1]
      ontologies << {:score=>oscore[oid], :ontologyId=>oid, :name=>name, :abbreviation=>abbr, :uri=>"http://bioportal.bioontology.org/ontologies/#{oid}"}
    end

    ontologies
  end


  def ontohtml (devel = 0)
    str_mtids = Hash.new {|h,k| h[k]=[]}       # matched tids per str
    @rterms.each {|str, exp, rel, tid, oid, sco| str_mtids[str + '-' + oid.to_s] << [tid, rel, sco]}

    htable = %Q(<form name="query" method="post" action="/owl" enctype="multipart/form-data">)
    
    htable += %Q(<table class="data draggable">\n)

    ## the header row
    row = 0
    htable += %Q(<tr id="row0">\n)
    htable += %Q(<th>Input terms</th>\n)
    htable += %Q(<th class="dropArea">Selected terms<br/><div class="allClearBtn">clear</div></th>\n)
    @oids.each do |oid|
      htable += %Q(<th>)
      htable += %Q(<div class="hideBtn"><img src="images/close_btn.gif"/></div>)
      htable += %Q(<div class="allDropBtn"><img src="images/drop_btn.jpg"></div>)
      htable += %Q(#{onto_format(oid)})
      htable += %Q(</th>\n)
    end
    htable += %Q{</tr>\n}
 
    ## the data rows
    @strs.each do |str|
      row += 1
      htable += %Q(<tr id="row#{row}">\n)
      htable += %Q(<td class="termColumn"><div class="rowHideBtn"><img src="images/close_btn.gif"/></div>#{str}</td>\n)
      htable += %Q(<td class="dropArea"></td>\n)

      @oids.each do |oid|
        htable += %Q(<td>\n)
        str_mtids[str + '-' + oid.to_s].each do |tid, rel, sco|
          htable += term_format(tid, rel, sco, devel)
        end
        htable += %Q(</td>\n)
      end
      htable += %Q{</tr>\n}
    end

    htable += %Q(</table>)

htable +=  <<HTML
<table style="border:1px solid cyan">
<tr><th colspan="2" style="text-align:left">Export selected terms to an OWL file</th></tr>
<tr><td>Ontology URI</td><td><input type="text" name="uri" size="40" placeholder="default: blank"/></td></tr>
<tr><td>Filename</td><td><input type="text" name="filename" size="40" placeholder="filename.owl or leave it blank to output to the screen" /></td></tr>
<tr><td colspan="2" style="text-align:right"><input type="submit" value="SUBMIT" class="uri_btn" /></td></tr>
</table>
HTML
#    htable += %Q(<p class="ontoInput">Ontology URI <input type="text" name="uri" size="30" /> <input type="submit" value="OntoFactory" class="uri_btn" /></p>)
    htable += %Q(</form>)
  end


  def term_format (tid, rel, sco, devel = 0)
    html  = %Q(<div class="datum">\n)
    html += %Q(<div class="dropBtn"><img src="images/drop_btn.jpg"></div>)

    @ddb_get_term.execute(tid)
    @ddb_get_term.fetch do |r|
      uri, tid, label, oid = r[0], r[1], r[2], r[3]

      label.gsub!('_', '_&#8203'); # to enable word wrapping around underscores

      html += %Q(<a href="http://bioportal.bioontology.org/ontologies/#{oid}/?p=terms&conceptid=#{tid} ")
      if (@boxtip) then
        html += %Q(onmouseover="box.showTooltip(event,'<iframe frameborder=0 width=600 src=http://bioportal.bioontology.org/ajax/term_details/#{oid}?styled=false&conceptid=#{tid}></iframe>',1,600)")
      end
      html += %Q(>#{label}</a>\n)
      html += %Q([#{rel}] (#{sco}) #{uri}\n) if (devel == 1)
#      html += %Q([#{rel}] <font size="1">#{tid}</font>\n) if (devel == 1)
      html += %Q(<span class="ontoUri">#{label}^#{uri}</span>\n)
    end
    html += %Q(</div>\n)
  end


  def onto_format (oid)
    @ddb_get_ontology.execute(oid)
    r = @ddb_get_ontology.fetch
    name, abbr = r[0], r[1]

    name.gsub!('_', '_&#8203'); # to enable word wrapping around underscores
    %Q(<a class="onto" href="http://bioportal.bioontology.org/ontologies/#{oid}" title="#{name}">#{abbr}</a>)
  end

end


if __FILE__ == $0
  ## config file processing
  require 'parseconfig'
  config     = ParseConfig.new('./ontofinder.cfg')
  dbid       = config['DBID']
  dbpw       = config['DBPassword']
  dbname     = config['DBName']
  maxnum_rel = config['MaxNumMatchingRelative'].to_i
  maxnum_abs = config['MaxNumMatchingAbsolute'].to_i
  minscore   = config['MinScore'].to_i
  devel      = config['DevelopmentMode'].to_i
  vids       = config['Ontologies'].split.collect {|id| id.to_i}
  
  ## command line option processing
  require 'optparse'
  optparse = OptionParser.new do|opts|
    opts.banner = "Usage: ontofinder.rb [options]"

    opts.on('-n', '--maxnum Number', 'specifies the maximum number of matches per term.') do |n|
      maxnum_rel = n.to_i
    end

    opts.on('-s', '--minscore Score', 'specifies the minimum score for tresholding.') do |s|
      minscore = s.to_f
    end

    opts.on('-o', '--ontologies vids', 'specifies the ontologies to be searched.') do |o|
      vids = o.split(/[ ,:|]+/).map {|s| s.to_i}
    end

    opts.on('-d', '--devel 0|1', 'tells it to show information for development.') do |d|
      devel = d
    end
    
    opts.on('-h', '--help', 'displays this screen') do
      puts opts
      exit
    end
  end

  optparse.parse!

  ontofinder = Ontofinder.new(dbid, dbpw, dbname)
  ARGF.each do |e|
#    result = ontofinder.ontofind(e.chomp.split(/\t/), maxnum_rel, maxnum_abs, minscore, vids, devel)
    result = ontofinder.get_uris(e.chomp.split(/\t/), maxnum_rel, maxnum_abs, minscore, vids)
    p result
  end
end
